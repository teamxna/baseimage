FROM ubuntu:14.04 
RUN apt-get update && apt-get install -y aptitude && aptitude install -y \
    git \
    libxml2-dev \
    build-essential \
    make \
    gcc \
    python2.7-dev \
    libpq-dev \
    python-pip \
    supervisor build-dep python-psycopg2 postgresql-devel \
    libcurl4-openssl-dev rsyslog libxslt1-dev
